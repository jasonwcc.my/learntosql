# Learn to SQL

### Create new table
- If table exists, remove it first
``` bash
DROP TABLE IF EXISTS 'Item';
CREATE TABLE 'Item' ('id' BIGINT not null auto_increment primary key, 'description' varchar(100), 'done' BIT);
```

### Insert rows
- Insert rows into previously created table
```
INSERT INTO 'Item' ('id', 'description', 'done') VALUES (1, 'Pick up newspaper', 0);
INSERT INTO 'Item' ('id', 'description', 'done') VALUES (1, 'Buy groceries', 1);
```
